//
//  GamePlayViewController.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit
import CoreGraphics
import AVFoundation
import Google
import UnityAds

final class GamePlayViewController: GAITrackedViewController {
    
    @IBOutlet var progressView: DALabeledCircularProgressView!
    @IBOutlet var picView: UIView!
    @IBOutlet var scoreView: UIView!
    @IBOutlet var rightAnsView: UIView!
    @IBOutlet var wrongAnsView: UIView!
    @IBOutlet var optionOneBtn: UIButton!
    @IBOutlet var optionTwoBtn: UIButton!
    @IBOutlet var optionThreeBtn: UIButton!
    @IBOutlet var skipBtn: UIButton!
    @IBOutlet var facebookButton: UIButton!
    @IBOutlet var startAgainButton: UIButton!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var bestScoreLbl: UILabel!
    @IBOutlet var scoreLbl: UILabel!
    @IBOutlet var oopsLbl: UILabel!
    @IBOutlet var msgLbl: UILabel!
    
    @IBOutlet var gameOverview: UIView!
    @IBOutlet var rightAnswersLbl: UILabel!
    @IBOutlet var skipsLbl: UILabel!
    @IBOutlet var endScoreLbl: UILabel!
    @IBOutlet var correctLbl: UILabel!
    @IBOutlet var wrongLbl: UILabel!
    @IBOutlet weak var bannerView: BannerView!
    
    var time = 100
    var rightCount = 0
    var skipCount = 0
    var score = 0
    var oldProgress = 100.0
    var timerExpired = false
    var answered = false
    var forLevel = false
    var animatingRightScreen = false
    var animatingWrongScreen = false
    var isGameEndUnityAdLoad = false
    
    var timer: Timer?
    var currentQuestion: Question?
    
    var optionSoundPlayer : AVAudioPlayer?
    var correctAnsPlayer : AVAudioPlayer?
    var wrongAnsPlayer : AVAudioPlayer?
    var timerBeepPlayer : AVAudioPlayer?
    // Interstitial Ad
    var interstitial: GADInterstitial!
    let shareController = ShareController()
    let interstitialController = GADInterstitialViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shareController.delegate = self
        score = 0
        rightCount = 0
        skipCount = 0
        time = 100
        UnityAds.initialize("1698273", delegate: self)
        
        
        self.configure()
        interstitialController.delegate = self
        self.interstitial = interstitialController.createAndLoadInterstitial()
        
        self.calculateScore()
        DataStorage.shared.deleteAnsweredQuestion(forLevel: self.forLevel)
        DataStorage.shared.calculateNextQs(forLevel: self.forLevel)
        self.updateViewForNextQuestion()        
    }
    
    // MARK: Configurations
    
    func configure() {
        facebookButton.configureForFacebook()
        
        optionOneBtn.isExclusiveTouch = true
        optionTwoBtn.isExclusiveTouch = true
        optionThreeBtn.isExclusiveTouch = true
        skipBtn.isExclusiveTouch = true
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        switch (deviceIdiom) {
        case .pad:
            self.progressView!.progressLabel.font = UIFont(name: "BebasNeue", size: 32.0)
        default:
            self.progressView!.progressLabel.font = UIFont(name: "BebasNeue", size: 18.0)
        }
        
        self.progressView!.thicknessRatio = 0.07;
        self.progressView!.roundedCorners = 0;
        self.progressView!.progressTintColor = UIColor(red: 250.0/255.0, green: 250.0/255.0, blue:252.0/255.0, alpha: 1.0)
        self.progressView!.trackTintColor = UIColor(red: 112.0/255.0, green: 174.0/255.0, blue: 52.0/255.0, alpha: 1.0)
        self.progressView!.progressLabel.textColor = UIColor(red: 26.0/255.0, green: 114.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        
        
        if let timerBeep = self.setupAudioPlayerWithFile("beep-21", type:"mp3") {
            self.timerBeepPlayer = timerBeep
        }
        if let optionSoundPlayer = self.setupAudioPlayerWithFile("swoosh-2", type:"mp3") {
            self.optionSoundPlayer = optionSoundPlayer
        }
        if let correctPlayer = self.setupAudioPlayerWithFile("Correct-answer", type:"wav") {
            self.correctAnsPlayer = correctPlayer
        }
        if let wrongPlayer = self.setupAudioPlayerWithFile("Wrong-answer", type:"wav") {
            self.wrongAnsPlayer = wrongPlayer
        }
    }
    
    // MARK: AudioPlayers Setup Methods
    
    func setupAudioPlayerWithFile(_ file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1
        let path = Bundle.main.path(forResource: file as String, ofType: type as String)
        let url = URL(fileURLWithPath: path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    // MARK: IBActions
    @IBAction func optionButtonWasPressed(_ sender: Any) {
        let button = sender as! UIButton
        let title = button.title(for: UIControlState())
        answered = true;
        self.stopAnimation()
        
        if ((self.optionSoundPlayer?.isPlaying) != nil) {
            self.optionSoundPlayer?.stop()
        }
        
        self.adjustUserInteractionOfButtons(enabled: false)
        if title! == currentQuestion!.answer as String {
            rightCount += 1;
            score = score + 10;
            self.calculateScore()
            
            if (!DataStorage.shared.soundOff) {
                self.correctAnsPlayer?.play()
            }
            
            rightAnsView.fade(in: 0.1, delegate: self)
        }
        else {
            if (!DataStorage.shared.soundOff) {
                self.wrongAnsPlayer?.play()
            }
            
            wrongAnsView.fade(in: 0.1, delegate: self)
        }
    }
    
    @IBAction func skipButtonWasPressed(_ sender: Any) {
        skipCount += 1;
        answered = true;
        self.stopAnimation()
        DataStorage.shared.deleteAnsweredQuestion(forLevel: self.forLevel)
        if DataStorage.shared.checkForGameEnd(forLevel: self.forLevel) {
            if (skipCount > 0) {
                oopsLbl.text = "Game Over"
                msgLbl.text = "Keep playing to improve your score!"
                startAgainButton.setTitle("START AGAIN", for: .normal)
            }
            self.gameEnds()
        }
        else {
            DataStorage.shared.calculateNextQs(forLevel: self.forLevel)
            self.updateViewForNextQuestion()
        }
    }
    
    @IBAction func playAgainButtonWasPressed(_ sender: Any) {
//        DataStorage.shared.resetValues(forLevel: self.forLevel)
//        _ = self.navigationController?.popViewController(animated: true)
        if startAgainButton.title(for: .normal) == "GO TO NEXT LEVEL" {
            DataStorage.shared.level = DataStorage.shared.level + 1;
            DataStorage.shared.currentQs = -1;
            
            gameOverview.fadeOut(0.4, delegate: nil)
            self.calculateScore()
            DataStorage.shared.calculateNextQs(forLevel: self.forLevel)
            self.updateViewForNextQuestion()
        } else {
            DataStorage.shared.resetValues(forLevel: self.forLevel)
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func facebookButtonWasPressed(_ sender: Any) {
        let productName = (Bundle.main.infoDictionary! as NSDictionary).object(forKey: kCFBundleNameKey) as! String
        shareController.share(onFacebook: Constants.iTunesURL,
                              description: NSString(format: "I have scored %ld with %@. You wanna play?", self.score, productName) as String)
    }
    
    @IBAction func nextButtonWasPressed(_ sender: Any) {
        if (answered) {
            answered = false;
            if (!animatingRightScreen) {
                animatingRightScreen = true;
                rightAnsView.fadeOut(0.1, delegate: self)
                
                DataStorage.shared.deleteAnsweredQuestion(forLevel: self.forLevel)
                if DataStorage.shared.checkForGameEnd(forLevel: self.forLevel) {
                    if (skipCount > 0) {
                        oopsLbl.text = "Game Over"
                        msgLbl.text = "Keep playing to improve your score!"
                        startAgainButton.setTitle("START AGAIN", for: .normal)
                    }
                    else {
                        oopsLbl.text = "Wohoo!"
                        if forLevel {
                            if DataStorage.shared.level < DataStorage.shared.quizDict.keys.count {
                                msgLbl.text = "You have cleared the level!"
                                startAgainButton.setTitle("GO TO NEXT LEVEL", for: .normal)
                            } else {
                                msgLbl.text = "You have cleared all levels."
                                startAgainButton.setTitle("START AGAIN", for: .normal)
                            }
                            
                            if DataStorage.shared.user.currentLevel < DataStorage.shared.quizDict.keys.count {
                                DataStorage.shared.user.currentLevel = DataStorage.shared.user.currentLevel + 1
                            }
                        } else {
                            msgLbl.text = "You have finished the game!"
                            startAgainButton.setTitle("START AGAIN", for: .normal)
                        }
                    }
                    
                    self.gameEnds()
                }
                else {
                    DataStorage.shared.calculateNextQs(forLevel: self.forLevel)
                    self.updateViewForNextQuestion()
                }
                DataStorage.shared.saveToDisk()
            }
        }
    }
    
    @IBAction func viewScoreButtonWasPressed(_ sender: Any) {
        if (answered) {
            if self.interstitial.isReady {
                answered = false
                self.interstitial.present(fromRootViewController: self)
            } else {
                self.interstitial = self.interstitialController.createAndLoadInterstitial()
                if self.interstitial.isReady {
                    answered = false
                    self.interstitial.present(fromRootViewController: self)
                } else {
                    answered = false
                    if (!animatingWrongScreen) {
                        animatingWrongScreen = true;
                        wrongAnsView.fadeOut(0.1, delegate: self)
                        
                        oopsLbl.text = "Game Over"
                        msgLbl.text = "Keep playing to improve your score!"
                        startAgainButton.setTitle("START AGAIN", for: .normal)
                        self.gameEnds()
                    }
                }
            }
        }
    }
    
    func adjustUserInteractionOfButtons(enabled: Bool) {
        optionOneBtn.isUserInteractionEnabled = enabled
        optionTwoBtn.isUserInteractionEnabled = enabled
        optionThreeBtn.isUserInteractionEnabled = enabled
        skipBtn.isUserInteractionEnabled = enabled
    }
    
    // MARK: Game Play
    
    func updateViewForNextQuestion() {
        answered = false;
        self.animateOutSubviews()
        let qsNo = DataStorage.shared.currentQs
        let array = DataStorage.shared.quizDict[DataStorage.shared.level]
        currentQuestion = array?[qsNo]
        self.progressView!.progressLabel.text = "10"
    }
    
    func animateOutSubviews() {
        self.adjustUserInteractionOfButtons(enabled: false)
        imgView.slideOut(to: kFTAnimationRight, duration: 0.3, delegate: nil)
        scoreView.slideOut(to: kFTAnimationRight, duration: 0.3, delegate: nil)
        
        optionOneBtn.slideOut(to: kFTAnimationRight, duration: 0.3, delegate: nil)
        optionTwoBtn.slideOut(to: kFTAnimationRight, duration: 0.3, delegate: nil)
        optionThreeBtn.slideOut(to: kFTAnimationRight, duration: 0.3, delegate: nil)
        skipBtn.slideOut(to: kFTAnimationRight, duration: 0.3, delegate: self)
    }
    
    func animateInSubviews() {
        imgView.slideIn(from: kFTAnimationLeft, duration: 0.3, delegate: nil)
        scoreView.slideIn(from: kFTAnimationLeft, duration: 0.3, delegate: self)
        
        var imgName = currentQuestion!.answer.lowercased()
        imgName = imgName.replacingOccurrences(of: " ", with: "_")
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        switch (deviceIdiom) {
        case .pad:
            imgName = NSString(format: "%@_pad.jpg", imgName) as String
        default:
            imgName = NSString(format: "%@.jpg", imgName) as String
        }
        
        let image = UIImage(named: imgName)
        imgView.image = image
    }
    
    func calculateScore() {
        scoreLbl.text = NSString(format: "%ld", score) as String
        if (score > DataStorage.shared.user.bestScore) {
            DataStorage.shared.user.bestScore = score
        }
        
        let bestScore = NSString(format: "%ld", DataStorage.shared.user.bestScore) as String
        bestScoreLbl.text = bestScore
    }
    
    func gameEnds() {
        self.stopAnimation()
        self.adjustUserInteractionOfButtons(enabled: false)
        
        endScoreLbl.text = NSString(format: "Score: %ld ", score) as String
        rightAnswersLbl.text = NSString(format: "%d Right Answers", rightCount) as String
        skipsLbl.text =  NSString(format: "%d Skips", skipCount) as String
        gameOverview.fade(in: 0.4, delegate: nil)
        isGameEndUnityAdLoad = true
    }
    
    func restartGame() {
        score = 0;
        rightCount = 0;
        skipCount = 0;
        self.calculateScore()
        DataStorage.shared.resetValues(forLevel: self.forLevel)
        DataStorage.shared.calculateNextQs(forLevel: self.forLevel)
        self.updateViewForNextQuestion()
    }
    
    // MARK: Timer Methods
    func startAnimation() {
        self.stopAnimation()
        
        rightAnsView.isHidden = true
        wrongAnsView.isHidden = true
        self.progressView!.isHidden = false
        
        self.progressView!.progress = 0.0;
        self.progressView!.progressLabel.text = "0";
        if (timer == nil) {
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(GamePlayViewController.progressChange), userInfo: nil, repeats: true)
            timer?.fire()
        }
    }
    
    func stopAnimation() {
        self.progressView!.progress = 0.0;
        self.progressView!.progressLabel.text = "0";
        self.progressView!.isHidden = true
        timer?.invalidate()
        
        if ((self.timerBeepPlayer?.isPlaying) != nil) {
            self.timerBeepPlayer?.stop()
        }
        
        timer = nil;
        time = 100;
    }
    
    func progressChange() {
        if (self.progressView!.progress < 1.0) {
            self.progressView!.setProgress(self.progressView!.progress + 0.01, animated: true)
            let progess = 10.0 - (self.progressView!.progress * 10)
            self.progressView!.progressLabel.text = String(format: "%.0f", progess)
            
            let pStr = String(format: "%.1f", progess) as NSString
            let pStrFloat = pStr.floatValue
            if (Float(progess) == truncf(Float(progess)) || pStrFloat == truncf(pStrFloat)) {
                if (!DataStorage.shared.soundOff) {
                    timerBeepPlayer!.play()
                }
            }
        } else {
            timer?.invalidate()
            timer = nil;
            self.progressView!.progress = 0.0;
            self.progressView!.progressLabel.text = "0";
            self.stopAnimation()
            oopsLbl.text = "Game Over"
            msgLbl.text = "Keep playing to improve your score!"
            startAgainButton.setTitle("START AGAIN", for: .normal)
            self.gameEnds()
        }
    }
    
    // MARK: UIViewController LifeCycle    
    override func viewWillAppear(_ animated: Bool) {
        self.screenName = "Game Screen"
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
    }
}

// MARK: CAAnimationDelegate Conformance
extension GamePlayViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        let targetView = anim.value(forKey: kFTAnimationTargetViewKey) as! UIView
        if (targetView == scoreView) {
            optionOneBtn.setTitle(currentQuestion!.optionOne as String, for: UIControlState())
            optionOneBtn.pop(in: 0.3, delegate: self)
            if (!DataStorage.shared.soundOff) {
                self.optionSoundPlayer!.play()
            }
        }
        else if (targetView == optionOneBtn) {
            optionTwoBtn.setTitle(currentQuestion!.optionTwo as String, for: UIControlState())
            optionTwoBtn.pop(in: 0.3, delegate: self)
            if (!DataStorage.shared.soundOff) {
                self.optionSoundPlayer!.play()
            }
        }
        else if (targetView == optionTwoBtn) {
            optionThreeBtn.setTitle(currentQuestion!.optionThree as String, for: UIControlState())
            optionThreeBtn.pop(in: 0.3, delegate: self)
            if (!DataStorage.shared.soundOff) {
                self.optionSoundPlayer!.play()
            }
        }
        else if (targetView == optionThreeBtn) {
            skipBtn.pop(in: 0.3, delegate: nil)
            if (!DataStorage.shared.soundOff) {
                self.optionSoundPlayer!.play()
            }
            self.startAnimation()
            self.adjustUserInteractionOfButtons(enabled: true)
        }
        else if (targetView == skipBtn) {
            self.animateInSubviews()
        }
        else if (targetView == rightAnsView) {
            if (!animatingRightScreen) {
                if (!rightAnsView.isHidden) {
                    self.perform(#selector(GamePlayViewController.nextButtonWasPressed(_:)), with: nil, afterDelay: 1.0)
                }
            }
            else {
                animatingRightScreen = false;
            }
        }
        else if (targetView == wrongAnsView) {
            if (!animatingWrongScreen) {
                if (!wrongAnsView.isHidden) {
                    self.perform(#selector(GamePlayViewController.viewScoreButtonWasPressed(_:)), with: nil, afterDelay: 1.0)
                }
            }
            else {
                animatingWrongScreen = false;
            }
        }
    }
}

// MARK: ShareControllerDelegate Conformance
extension GamePlayViewController: ShareControllerDelegate {
    internal func errorReceived(message: String) {
        self.showAlert("Error", message: message, okTitle: "OK", delegate: self)
    }
}

// MARK: GADInterstitialDelegate Conformance
extension GamePlayViewController: GADInterstitialControllerDelegate {
   
    internal func interstitialDidReceiveAd(_ ad: GADInterstitial) {
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.interstitial = interstitialController.createAndLoadInterstitial()
        if (!animatingWrongScreen) {
            animatingWrongScreen = true;
            wrongAnsView.fadeOut(0.1, delegate: self)
            
            oopsLbl.text = "Game Over"
            msgLbl.text = "Keep playing to improve your score!"
            startAgainButton.setTitle("START AGAIN", for: .normal)
            self.gameEnds()
            isGameEndUnityAdLoad = false
        }
    }
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        let placement = "video"
        if (UnityAds.isReady(placement) && isGameEndUnityAdLoad) {
            UnityAds.show(self, placementId: placement)
        }
    }

}



extension GamePlayViewController: UnityAdsDelegate {
    func unityAdsReady(_ placementId: String) {
        //UnityAds.show(self, placementId: placementId)
    }
    
    func unityAdsDidStart(_ placementId: String) { }
    
    func unityAdsDidError(_ error: UnityAdsError, withMessage message: String) { }
    
    func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) { }
    
}
