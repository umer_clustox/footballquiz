//
//  Question.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import UIKit

class Question {
    var optionTwo = ""
    var optionOne = ""
    var optionThree = ""
    var answer = ""
    var level: Int = -1
    
    init(dictionary : NSDictionary) {
        self.optionOne = dictionary.object(forKey: "option1") as! String
        self.optionTwo = dictionary.object(forKey: "option2") as! String
        self.optionThree = dictionary.object(forKey: "option3") as! String
        self.answer = dictionary.object(forKey: "answer") as! String
        self.level = Int((dictionary.object(forKey: "level") as! String))!
        
        let iPhoneImage = UIImage(named: "\(self.answer.lowercased().replacingOccurrences(of: " ", with: "_")).jpg")
        if iPhoneImage == nil {
            print("Missing Image Iphone: \(self.answer.lowercased().replacingOccurrences(of: " ", with: "_")).jpg")
        }
        
        let iPadImage = UIImage(named: "\(self.answer.lowercased().replacingOccurrences(of: " ", with: "_"))_pad.jpg")
        if iPadImage == nil {
            print("Missing Image Ipad: \(self.answer.lowercased().replacingOccurrences(of: " ", with: "_"))_pad.jpg\n")
        }
    }
}
