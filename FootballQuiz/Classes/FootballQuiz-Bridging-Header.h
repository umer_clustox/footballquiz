//
//  GuessTheFootballPlayer-Bridging-Header.h
//  GuessTheFootballPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

#ifndef FootballQuiz_Bridging_Header_h
#define FootballQuiz_Bridging_Header_h

#import <Google/Analytics.h>
#import <GoogleAnalytics/GAILogger.h>
#import "GAI.h"
#import "FTAnimation.h"

#import <GoogleMobileAds/GADBannerViewDelegate.h>
#import <GoogleMobileAds/GADInterstitialDelegate.h>
#import <GoogleMobileAds/GADBannerView.h>
#import <GoogleMobileAds/GADInterstitial.h>

#import "DALabeledCircularProgressView.h"

#endif /* FootballQuiz_Bridging_Header_h */
