//
//  Constants.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation

/**
 *  Struct for declaring this project's global values
 */
struct Constants {
    // Banner id for Admob
    static let admobBannerID = "ca-app-pub-8793358717797604/9163669083"
    
    // Interstitial Id for Admob
    static let admobInterstitialID = "ca-app-pub-8793358717797604/1640402286"
    
    // Admob Application ID
    static let googleApplicationID = "ca-app-pub-8793358717797604~7686935888"
    
    // Google Analytics ID
    static let googleAnalyticsID = "UA-55757458-9"
    
    // Appstore ID of this App
    static let appID = "973957175"
    
    // iTunes URL of App
    static let iTunesURL = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=\(appID)&mt=8&uo=6"
    
    // URL hosting more apps file
    static let moreAppsFileURL = ""
}
