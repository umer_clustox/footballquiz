//
//  HomeScreenViewController.swift
//  GuessTheFlag
//
//  Created by Saira on 12/28/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit
import Google

class HomeScreenViewController: GAITrackedViewController {
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var levelsButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var bannerView: BannerView!
    let shareController = ShareController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookButton.configureForFacebook()
        self.enablePlayButtonsWhenQuizIsReady()
        
        shareController.delegate = self
    }
    
    @IBAction func facebookButtonWasPressed(_ sender: Any) {
        shareController.share(onFacebook: Constants.iTunesURL,
                              description: "")
    }

    func enablePlayButtonsWhenQuizIsReady() {
        DispatchQueue.main.async {
            if (self.startButton != nil) {
                if DataStorage.shared.quizDict.count > 0 {
                    self.startButton.isEnabled = true
                    self.levelsButton.isEnabled = true
                } else {
                    self.startButton.isEnabled = false
                    self.levelsButton.isEnabled = false
                }
            }
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "ShowGamePlaySegue" {
            guard let controller = segue.destination as? GamePlayViewController else {
                    return
            }
            controller.forLevel = false
            DataStorage.shared.level = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.screenName = "Home Screen";
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: ShareControllerDelegate Conformance
extension HomeScreenViewController: ShareControllerDelegate {
    internal func errorReceived(message: String) {
        self.showAlert("Error", message: message, okTitle: "OK", delegate: self)
    }
}
